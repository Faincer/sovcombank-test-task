import django.forms as forms

from .models import Application

class ApplicationForm(forms.ModelForm):

    class Meta:
        model = Application
        fields = '__all__'
        exclude = ('_is_edit',)
        widgets = {'telephone': forms.TextInput(attrs={'data-mask': "0000000000"}),
                    'date': forms.widgets.DateInput(attrs={'type': 'date'})
                    }