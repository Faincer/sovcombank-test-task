from django.contrib import admin

from .models import Application

from simple_history.admin import SimpleHistoryAdmin


admin.site.register(Application, SimpleHistoryAdmin)
# Register your models here.
