import csv
import datetime

from django.db.models import Q
from django.utils import timezone 
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import (
    DeleteView, 
    DetailView, 
    ListView, 
    CreateView, 
    UpdateView
    )
from .models import Application
from .forms import ApplicationForm
# Create your views here.

applications = Application.objects.all()
TIMEOUT = 180


class ApplicationCreateView(LoginRequiredMixin, CreateView):
    model = Application
    form_class = ApplicationForm
    template_name = 'app/create.html'
    success_url = reverse_lazy('app-list')
    

class ApplicationUpdateView(LoginRequiredMixin, UpdateView):
    model = Application
    form_class = ApplicationForm
    success_url = reverse_lazy('app-list')

    def get(self, request, **kwargs):
        context = {}
        app = get_object_or_404(Application, pk=kwargs['pk'])
        context['form'] = ApplicationForm(instance=app)
        context['application'] = app
        context['history_diff'] = app.history_diff()
        now = timezone.now()
        timeout = app.updated_at and (now-app.updated_at).total_seconds()>TIMEOUT
        if app._is_edit==False or timeout:
            app.updated_at = now
            app._is_edit = True

            app.save()
            return render(request, 'app/update.html', context)
        else:
            return HttpResponseForbidden("This line is already editting")

    def post(self, request, **kwargs):
        app = get_object_or_404(Application, pk=kwargs['pk'])
        app._is_edit=False
        if not ('True' in request.POST and request.POST['True']=='Cancel'):
            form = ApplicationForm(request.POST, instance=app)
            if form.is_valid():
                form.save()
        else:
            app.save()
        return HttpResponseRedirect(reverse_lazy('app-list'))


class ApplicationDeleteView(LoginRequiredMixin, DeleteView):
    model = Application
    template_name = 'app/delete.html'
    success_url = reverse_lazy('app-list')


class ApplicationListView(LoginRequiredMixin, ListView):
    model = Application
    template_name = 'app/list.html'

    def get(self, request, **kwargs):
        global applications
        context = {}
        if 'save_excel' in request.GET:
            return save_excel(request, applications)
        if 'input_search' in request.GET:
            applications = Application.objects.all()
            if request.GET['input_search'] != '':
                applications = applications.filter(
                    Q(date__contains=request.GET['input_search']) |
                    Q(product__contains=request.GET['input_search']) |
                    Q(telephone__contains=request.GET['input_search']) |
                    Q(solve__contains=request.GET['input_search']) |
                    Q(comment__contains=request.GET['input_search'])
                )
        if 'sort' in request.GET:
            applications = applications.order_by(request.GET['sort'])

        if request.META['QUERY_STRING'] == '':
            applications = Application.objects.all()
        context['object_list'] = applications
        return render(request, 'app/list.html', context)


def save_excel(request, applications):
    # Create the HttpResponse object with the appropriate CSV header.
    need_save_fields = ('date', 'product', 'telephone', 'solve', 'comment')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="table.csv"'
    
    app_file = csv.writer(response)
    fieldnames = [
        f.name for f in Application._meta.get_fields() if f.name in need_save_fields
        ]
    app_file.writerow(fieldnames)
    for line in applications:
        app_file.writerow([line.__dict__[fieldname] for fieldname in fieldnames])

    return response