from django.apps import AppConfig


class ApplicationTableConfig(AppConfig):
    name = 'application_table'
