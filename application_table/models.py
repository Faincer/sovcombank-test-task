from django.db import models

from simple_history.models import HistoricalRecords

# Create your models here.
class Application(models.Model):
    date = models.DateField(auto_now_add=True)
    product = models.CharField(max_length=30)
    telephone = models.CharField(max_length=10)
    solve = models.CharField(max_length=50, blank=True)
    comment = models.CharField(max_length=100, blank=True)

    _is_edit = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def history_diff(self):
        diff = []
        history = self.history.all()
        for i in range(1,len(history)):
            new_record, old_record = history[i],history[i-1]
            delta = new_record.diff_against(old_record)
            for change in delta.changes:
                if change.field not in ['_is_edit','updated_at']:
                    diff.append([
                        new_record.history_date, 
                        "{} changed from {} to {}".format(
                            change.field,
                            change.old,
                            change.new
                            )
                        ])

        return diff
